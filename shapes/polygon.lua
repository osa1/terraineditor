local Class      = require "hump.class"
local ShapePoint = require "shapes.shapepoint"
local HCShape    = require "HardonCollider.shapes"

require "util"

local PolygonShape = Class {}
PolygonShape.name = "polygon"

function PolygonShape:init()
    self.shapePoints = {}
    self.centerPoint = nil
    self._lastCenterPoint = nil
    self._hcshape = nil
    self._dragging = false
end

function PolygonShape:update(dt)
    for _, p in ipairs(self.shapePoints) do
        p:update(dt)
    end

    if self.centerPoint then self.centerPoint:update(dt) end

    if self._dragging then
        local cp = self.centerPoint.pos
        local lcp = self._lastCenterPoint

        local dx = cp.x - lcp.x
        local dy = cp.y - lcp.y

        for _, p in ipairs(self.shapePoints) do
            p.pos.x = p.pos.x + dx
            p.pos.y = p.pos.y + dy
        end

        lcp.x = cp.x
        lcp.y = cp.y
    end
end

function PolygonShape:draw()
    local len = #self.shapePoints
    assert(len <= 8)
    if len >= 3 then
        local points = {}
        for _, p in ipairs(self.shapePoints) do
            points[#points+1] = p.pos.x
            points[#points+1] = p.pos.y
        end
        love.graphics.polygon("line", unpack(points))
    end

    for _, p in ipairs(self.shapePoints) do
        p:draw()
    end
    if self.centerPoint then self.centerPoint:draw() end
end

function PolygonShape:_updatehcshape()
    local points = {}
    for _, p in ipairs(self.shapePoints) do
        points[#points+1] = p.pos.x
        points[#points+1] = p.pos.y
    end

    local err, ret = pcall(HCShape.newPolygonShape, unpack(points))
    if not err then
        print("error:", ret)
        self._hcshape = nil
        self.centerPoint = nil
    else
        self._hcshape = ret
        local cx, cy = ret:center()
        self.centerPoint = ShapePoint(0, cx, cy)
    end
end

function PolygonShape:mousepressed(x, y, button)
    local cp = self.centerPoint
    if cp and cp:mousepressed(x, y, button) then
        self._dragging = true
        self._lastCenterPoint = { x = cp.pos.x, y = cp.pos.y }
        return true
    end

    for _, p in ipairs(self.shapePoints) do
        if p:mousepressed(x, y, button) then return true end
    end

    if button == "l" then
        local len = #self.shapePoints
        if len < 8 then
            self.shapePoints[len+1] = ShapePoint(len + 1, x, y)
        else
            self.shapePoints = { ShapePoint(1, x, y) }
            self.centerPoint = nil
        end
        if #self.shapePoints >= 3 then
            self:_updatehcshape()
        end
        return true
    end
end

function PolygonShape:mousereleased(x, y, button)
    for _, p in ipairs(self.shapePoints) do
        if p:mousereleased(x, y, button) then
            self:_updatehcshape()
            return true
        end
    end

    local cp = self.centerPoint
    if cp and cp:mousereleased(x, y, button) then
        self._dragging = false
        return true
    end
end

function PolygonShape:keypressed(key, unicode)

end

function PolygonShape:keyreleased(key, unicode)

end

return PolygonShape
