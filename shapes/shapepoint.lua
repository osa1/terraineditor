local Class = require "hump/class"

require "util"

local ShapePoint = Class {}

function ShapePoint:init(id, posx, posy, size)
    self.id       = id
    self.pos      = { x = posx, y = posy }
    self.size     = size or 5
    self._clicked = false
end

function ShapePoint:click()
    self._clicked = true
end

function ShapePoint:unclick()
    self._clicked = false
end

function ShapePoint:update(dt)
    if self._clicked then
        local x, y = love.mouse.getWorldPosition()
        self.pos.x = x
        self.pos.y = y
    end
end

function ShapePoint:draw()
    local pos = self.pos

    love.graphics.rectangle("fill", pos.x - self.size/2, pos.y - self.size/2, self.size, self.size)

    if self.id then
        local font = love.graphics.getFont()
        local h    = font:getHeight()
        local w    = font:getWidth(self.id)

        love.graphics.getFont():getHeight()
        love.graphics.print(self.id, pos.x - w/2, pos.y - 2*h)
    end
end

function ShapePoint:mousepressed(x, y, button)
    if button == "l" then
        if insideRect_center(self.pos.x, self.pos.y, self.size, self.size, x, y) then
            self:click()
            return true
        end
    end
end

function ShapePoint:mousereleased(x, y, button)
    if button == "l" then
        if self._clicked then
            self._clicked = false
            return true
        end
    end
end

function ShapePoint.range(p1, p2)
    local dx = p1.pos.x - p2.pos.x
    local dy = p1.pos.y - p2.pos.y
    return math.sqrt(dx * dx + dy * dy)
end

return ShapePoint
