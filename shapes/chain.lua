local Class      = require "hump/class"
local ShapePoint = require "shapes/shapepoint"

local ChainShape = Class {}
ChainShape.name = "chain"

function ChainShape:init()
    self.points = {}
end

function ChainShape:update(dt)
    for _, p in ipairs(self.points) do
        p:update(dt)
    end
end

function ChainShape:draw()
    for i=1, #self.points do
        if i == #self.points then
            self.points[i]:draw()
            break
        end

        local start = self.points[i]
        local end_  = self.points[i+1]

        love.graphics.line(start.pos.x, start.pos.y, end_.pos.x, end_.pos.y)
        start:draw()
    end
end

function ChainShape:mousepressed(x, y, button)
    for _, p in ipairs(self.points) do
        if p:mousepressed(x, y, button) then
            return true
        end
    end

    local len = #self.points
    self.points[len+1] = ShapePoint(len, x, y)
end

function ChainShape:mousereleased(x, y, button)
    for _, p in ipairs(self.points) do
        if p:mousereleased(x, y, button) then
            return true
        end
    end
end

function ChainShape:keypressed(key, unicode)

end

function ChainShape:keyreleased(key, unicode)

end

return ChainShape
