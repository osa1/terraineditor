local Class      = require "hump.class"
local ShapePoint = require "shapes.shapepoint"

local LightSource = Class {}
LightSource.name = "light source"

function LightSource:init()
    self.centerPoint = nil
    self._lastCenterPoint = nil
    self._dragging = false
end

function LightSource:update(dt)
    if self.centerPoint then self.centerPoint:update(dt) end

    if self._dragging then
        local cp  = self.centerPoint.pos
        local lcp = self._lastCenterPoint

        local dx = cp.x - lcp.x
        local dy = cp.y - lcp.y

        lcp.x = cp.x
        lcp.y = cp.y
    end
end

function LightSource:draw()
    if self.centerPoint then
        love.graphics.rectangle("fill", self.centerPoint.pos.x - 5, self.centerPoint.pos.y - 5, 10, 10)
    end
end

function LightSource:mousepressed(x, y, button)
    local cp = self.centerPoint
    if not cp then
        self.centerPoint = ShapePoint(0, x, y)
        return true
    elseif cp:mousepressed(x, y, button) then
        self._dragging = true
        self._lastCenterPoint = { x = cp.pos.x, y = cp.pos.y }
        return true
    end
end

function LightSource:mousereleased(x, y, button)
    local cp = self.centerPoint
    if cp and cp:mousereleased(x, y, button) then
        self._dragging = false
        return true
    end
end

function LightSource:keypressed(key, unicode)

end

function LightSource:keyreleased(key, unicode)

end

return LightSource
