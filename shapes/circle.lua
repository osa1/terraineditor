local Class      = require "hump/class"
local ShapePoint = require "shapes/shapepoint"

local CircleShape = Class {}
CircleShape.name = "circle"

function CircleShape:init()
    self.centerPoint = nil
    self.rPoint      = nil
    self._lastCenterPoint = nil
    self._dragging = false
end

function CircleShape:getRadius()
    assert(self.centerPoint and self.rPoint, "circle is not defined")

    local c = self.centerPoint.pos
    local r = self.rPoint.pos

    local dx = c.x - r.x
    local dy = c.y - r.y

    return math.sqrt(dx*dx + dy*dy)
end

function CircleShape:update(dt)
    if self.centerPoint then self.centerPoint:update(dt) end

    if self._dragging then
        local cp  = self.centerPoint.pos
        local lcp = self._lastCenterPoint

        local dx = cp.x - lcp.x
        local dy = cp.y - lcp.y

        local rp = self.rPoint.pos
        rp.x = rp.x + dx
        rp.y = rp.y + dy

        lcp.x = cp.x
        lcp.y = cp.y
    end

    if self.rPoint then self.rPoint:update(dt) end
end

function CircleShape:draw()
    if self.centerPoint then
        local pos = self.centerPoint.pos
        local x, y = love.mouse.getWorldPosition()

        if self.rPoint then
            love.graphics.circle("line", pos.x, pos.y, ShapePoint.range(self.centerPoint, self.rPoint))
        else
            love.graphics.circle("line", pos.x, pos.y, ShapePoint.range(self.centerPoint, ShapePoint(nil, x, y, nil)))
        end
    end

    if self.centerPoint then self.centerPoint:draw() end
    if self.rPoint      then self.rPoint:draw()      end
end

function CircleShape:mousepressed(x, y, button)
    local cp = self.centerPoint
    if cp and cp:mousepressed(x, y, button) then
        self._dragging = true
        self._lastCenterPoint = { x = cp.pos.x, y = cp.pos.y }
        return
    end
    if self.rPoint and self.rPoint:mousepressed(x, y, button) then
        return
    end

    if button == "l" then
        if self.centerPoint == nil then
            self.centerPoint = ShapePoint(nil, x, y)
            if self.onfocus then self.onfocus() end
        elseif self.rPoint == nil then
            self.rPoint = ShapePoint(nil, x, y)
            if self.onfocuslost then self.onfocuslost() end
        else
            self.centerPoint = ShapePoint(nil, x, y)
            self.rPoint = nil
            if self.onfocus then self.onfocus() end
        end
    end
end

function CircleShape:mousereleased(x, y, button)
    if button == "l" then
        if self.centerPoint then
            if self.centerPoint:mousereleased(x, y, button) then
                self._dragging = false
                return true
            end
        end
        if self.rPoint then
            if self.rPoint:mousereleased(x, y, button) then
                return true
            end
        end
    end
end

function CircleShape:keypressed(key, unicode)

end

function CircleShape:keyreleased(key, unicode)

end

return CircleShape
