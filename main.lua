require "LoveFrames"

local Shape   = require "shape"
local Camera  = require "hump/camera"
local Export  = require "export"

local cam

local scrollSpeed = 350 -- px / second
local zoomSpeed   = 10

function love.load()
    love.graphics.setMode(1000, 700, false, false, 0)
    cam = Camera(4000, 2000)

    function love.mouse.getWorldPosition()
        return cam:mousepos()
    end
end

function love.update(dt)
    local dx, dy = 0, 0
    local down = love.keyboard.isDown

    if down("left")  then dx = -scrollSpeed * dt end
    if down("right") then dx =  scrollSpeed * dt end
    if down("up")    then dy = -scrollSpeed * dt end
    if down("down")  then dy =  scrollSpeed * dt end

    cam:move(dx, dy)

    Shape.update(dt)
    loveframes.update(dt)
end

function love.draw()
    if cam.scale < 0.3 then
        cam.scale = 0.3
    elseif cam.scale > 3 then
        cam.scale = 3
    end
    cam:attach()
    Shape.draw()
    cam:detach()
    loveframes.draw()
    love.graphics.setCaption(love.timer.getFPS())
end

function love.mousepressed(x, y, button)
    if Shape.hasFocus() or not loveframes.mousepressed(x, y, button) then
        local dt = love.timer.getDelta()
        local width = love.graphics.getWidth()
        local height = love.graphics.getHeight()

        local dx = x - width / 2
        local dy = y - height / 2

        if button == "wu" then
            cam.scale = cam.scale + zoomSpeed * dt
            cam:move(dx / zoomSpeed, dy / zoomSpeed)
        elseif button == "wd" then
            local dt = love.timer.getDelta()
            cam.scale = cam.scale - zoomSpeed * dt
            cam:move(dx / zoomSpeed, dy / zoomSpeed)
        else
            x, y = cam:mousepos()
            Shape.mousepressed(x, y, button)
        end
    end
end

function love.mousereleased(x, y, button)
    local xc, yc = cam:worldCoords(x, y)
    Shape.mousereleased(xc, yc, button)
    loveframes.mousereleased(x, y, button)
end

function love.keypressed(key, unicode)
    if key == "escape" then love.event.push("quit") end
    if key == "e" then
        Export.export("TEoutput.lua")
        return
    end
    --if Shape.hasFocus() or not loveframes.keypressed(key, unicode) then
        --Shape.keypressed(key, unicode)
    --end
    loveframes.keypressed(key, unicode)
end

function love.keyreleased(key, unicode)
    Shape.keyreleased(key, unicode)
    loveframes.keyreleased(key, unicode)
end
