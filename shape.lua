local ShapeButtonList = require "gui/shapebuttonlist"
local Shadow          = require "shadow"

require "null"

local Shape = {}

Shape.shapes    = {}
Shape._hasFocus = false
Shape.selected  = null

function Shape.hasFocus()
    return Shape._hasFocus
end

function Shape.draw()
    for _, shape in ipairs(Shape.shapes) do
        shape:draw()
        if shape.centerPoint and shape.name == "light source" then
            for _, shadowCaster in ipairs(Shape.shapes) do
                if shadowCaster.name ~= "light source" then
                    local vs = shadowCaster:castShadow(shape.centerPoint.pos)
                    if vs then love.graphics.polygon("fill", unpack(vs)) end
                end
            end
        end
    end
end

function Shape.update(dt)
    Shape.selected:update(dt)
end

function Shape.mousepressed(x, y, button)
    Shape.selected:mousepressed(x, y, button)
end

function Shape.mousereleased(x, y, button)
    Shape.selected:mousereleased(x, y, button)
end

function Shape.keypressed(key, unicode)
    Shape.selected:keypressed(key, unicode)
end

function Shape.keyreleased(key, unicode)
    Shape.selected:keyreleased(key, unicode)
end

Shape.btnlst = ShapeButtonList()

function Shape.btnlst.onadd(shapeBtn)
    shapeBtn.shape.onfocus = function ()
        Shape._hasFocus = true
    end

    shapeBtn.shape.onfocuslost = function ()
        Shape._hasFocus = false
    end

    for _, btn in ipairs(Shape.btnlst.shapeBtns) do
        btn:_setDeselectedText()
    end

    shapeBtn:_setSelectedText()

    table.insert(Shape.shapes, shapeBtn.shape)
    Shape.selected = shapeBtn.shape

    shapeBtn.btn.OnClick = function ()
        if shapeBtn._selected then
            shapeBtn:_setDeselectedText()
            Shape.selected = null
        else
            for _, btn in ipairs(Shape.btnlst.shapeBtns) do
                btn:_setDeselectedText()
            end
            shapeBtn:_setSelectedText()
            Shape.selected = shapeBtn.shape
        end
    end
end

return Shape
