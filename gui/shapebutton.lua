local Class = require "hump/class"

local ShapeButton = Class {}

function ShapeButton.init(self, idx, shape, parent)
    self.btn = loveframes.Create("button", parent)
    self.btn:SetText("Shape #" .. tostring(idx) .. " - " .. shape.name)
    self.btn.OnClick = self.onclick
    self.shape = shape
    self._selected = false
end

function ShapeButton:_setSelectedText()
    if not self._selected then
        self.btn:SetText(self.btn:GetText() .. " [x]")
        self._selected = true
    end
end

function ShapeButton:_setDeselectedText()
    if self._selected then
        local text = self.btn:GetText()
        self.btn:SetText(string.sub(text, 1, #text-4))
        self._selected = false
    end
end

function ShapeButton:toggleSelected()
    if self._selected then
        self._setDeselectedText()
    else
        self._setSelectedText()
    end
    self._selected = not self._selected
end

function ShapeButton.onclick() end

return ShapeButton
