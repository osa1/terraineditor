local Class         = require "hump/class"
local ShapeButton   = require "gui/shapebutton"
local ShapeTypeList = require "gui/shapetypelist"

local ShapeButtonList = Class {}

function ShapeButtonList.init(self)
    self.frame = loveframes.Create("frame")
    self.frame
        :SetName("Shapes")
        :SetSize(250, 200)
        :SetPos(100, 100)
        :SetModal(false)

    self.addBtn = loveframes.Create("button", self.frame)
    self.addBtn
        :SetText("Add shape")
        :SetSize(self.addBtn:GetParent():GetWidth(), 20)
        :SetPos(0, self.addBtn:GetParent():GetHeight() - self.addBtn:GetHeight())
    self.addBtn.OnClick = function () self.shapeTyList:setVisible(true) end

    self.list = loveframes.Create("list", self.frame)
    self.list
        :SetPos(0, 20)
        :SetSize(self.list:GetParent():GetWidth(),
            self.list:GetParent():GetHeight() - 19 - self.addBtn:GetHeight())
        :SetPadding(5)
        :SetSpacing(5)
        :SetAutoScroll(true)

    self.shapeBtns = {}

    self.shapeTyList = ShapeTypeList()
    self.shapeTyList.onselect = function (shape)
        local btns     = #self.shapeBtns
        local shapeBtn = ShapeButton(btns, shape)
        self.shapeBtns[btns+1] = shapeBtn
        self.list:AddItem(shapeBtn.btn)
        if self.onadd then self.onadd(shapeBtn) end
    end
end

return ShapeButtonList
