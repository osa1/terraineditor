local Class        = require "hump/class"

--local ChainShape   = require "shapes/chain"
local CircleShape  = require "shapes/circle"
local PolygonShape = require "shapes/polygon"
local LightSource  = require "shapes/lightsource"
local ChainShape   = require "shapes/chain"

local ShapeTypeList = Class {}

function ShapeTypeList:init()
    self.frame = loveframes.Create("frame")
    self.frame
        :SetName("Select shape type")
        :SetSize(150, 200)
        :SetPos(200, 100)
        :SetVisible(false)
        :SetModal(false)

    self.frame.OnClose = function ()
        -- FIXME: This part is broken because of LoveFrames' handing of close button.
        -- The frame is always removed from LoveFrames even if OnClose is overrided.
        --self:SetVisible(false)
            --:SetModal(false)
    end

    self.list = loveframes.Create("list", self.frame)
    self.list
        :SetPos(0, 20)
        :SetSize(self.list:GetParent():GetWidth(), self.list:GetParent():GetHeight() - 20)
        :SetPadding(5)
        :SetSpacing(5)

    local function close(f)
        return function ()
            f()
            self:setVisible(false)
        end
    end

    local function mkShapeBtn(name, constr)
        local btn = loveframes.Create("button", self.list)
        btn:SetText(name)
        btn.OnClick = close(function ()
            if self.onselect then
                self.onselect(constr())
            end
        end)
    end

    --mkShapeBtn("Chain", ChainShape)
    mkShapeBtn("Polygon", PolygonShape)
    mkShapeBtn("Circle", CircleShape)
    mkShapeBtn("Light Source", LightSource)
    mkShapeBtn("Chain", ChainShape)
end

function ShapeTypeList:setVisible(s)
    self.frame
        :SetVisible(s)
        :SetModal(s)
end

return ShapeTypeList
