local Export = {}

local Shape        = require "shape"
local PolygonShape = require "shapes/polygon"
local CircleShape  = require "shapes/circle"
local LightSource  = require "shapes/lightsource"
local ChainShape   = require "shapes/chain"

function Export.exportWorld()
    return table.concat({
        "local world = love.physics.newWorld(4000, 2000, false)",
        "local body  = love.physics.newBody(world, 2000, 1000, \"static\")"
    }, "\n")
end

function CircleShape:export(i)
    if not self.rPoint then return "" end

    local xOff = 4000 - self.centerPoint.pos.x
    local yOff = 2000 - self.centerPoint.pos.y

    return table.concat({
        "local circle" .. i .. " = love.physics.newCircleShape(" .. xOff .. ", " .. yOff .. ", " .. self:getRadius() .. ")",
        "local fixtureCircle" .. i .. " = love.physics.newFixture(body, circle" .. i .. ")"
    }, "\n")
end

function PolygonShape:export(i)
    local function concatPoints(vertices)
        local r = {}
        for i, p in ipairs(vertices) do
            local xOff = 4000 - p.x
            local yOff = 2000 - p.y

            r[2 * i - 1] = tostring(xOff)
            r[2 * i]     = tostring(yOff)
        end
        return table.concat(r, ", ")
    end

    if not self._hcshape then return "" end
    return table.concat({
        "local polygon" .. i .. " = love.physics.newPolygonShape(" .. concatPoints(self._hcshape._polygon.vertices) .. ")",
        "local fixturePolygon" .. i .. " = love.physics.newFixture(body, polygon" .. i .. ")"
    }, "\n")
end

function LightSource:export(i)
    return ""
end

function ChainShape:export(i)
    local function concatPoints(vertices)
        local r = {}
        for i, p in ipairs(vertices) do
            r[2 * i - 1] = tostring(p.pos.x)
            r[2 * i ]    = tostring(p.pos.y)
        end
        return table.concat(r, ", ")
    end

    return table.concat({
        "local chain" .. i .. " = love.physics.newChainShape(" .. concatPoints(self.points) .. ")",
        "local fixtureChain" .. i .. " = love.physics.newFixture(body, chain" .. i .. ")"
    }, "\n")
end

function Export.export(path)
    local r = { Export.exportWorld() }
    for i, shape in ipairs(Shape.shapes) do
        r[#r+1] = shape:export(i)
    end
    r = table.concat(r, "\n")
    if path then
        local file = io.open(path, "w")
        file:write(r)
        file:close()
    else
        print(r)
    end
end

return Export
