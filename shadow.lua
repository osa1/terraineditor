local Vector       = require "hump/vector"
local CircleShape  = require "shapes/circle"
local PolygonShape = require "shapes/polygon"
local ChainShape   = require "shapes/chain"
require "util"

local Shadow = {}
Shadow.shadowSize = 500

function PolygonShape:castShadow(p, shadowSize)
    if not self._hcshape then return end

    shadowSize = shadowSize or Shadow.shadowSize
    local points = self._hcshape._polygon.vertices

    local pos = Vector(p.x, p.y)

    local vertices = {}
    local isLastHidden = false

    local startv = Vector(points[1].x, points[1].y)
    local endv   = startv

    for i=1, #points do
        local v = points[i]
        v = Vector(v.x, v.y)
        local w = points[(i % #points) + 1]
        w = Vector(w.x, w.y)

        local c = .5 * (w + v)
        local n = (w - v):perpendicular()
            -- a vertex is hidden if it's back facing
        local isHidden = n * (c - pos) < 0

        if isHidden then
            if not isLastHidden then
                startv = v
            end
        elseif isLastHidden then
            endv = v
        end
        isLastHidden = isHidden
    end

    vertices[1] = startv.x
    vertices[2] = startv.y
    vertices[3] = endv.x
    vertices[4] = endv.y
    endv   = endv   + (endv - pos)   * shadowSize
    startv = startv + (startv - pos) * shadowSize
    vertices[5] = endv.x
    vertices[6] = endv.y
    vertices[7] = startv.x
    vertices[8] = startv.y

    return vertices
end

function CircleShape:castShadow(p)
    if not self.centerPoint or not self.rPoint then return end

    local c = self.centerPoint.pos
    local r = self.rPoint.pos

    -- FIXME: This is a bit buggy, think more.
    shadowSize = shadowSize or Shadow.shadowSize

    p  = Vector(p.x, p.y)
    c  = Vector(c.x, c.y)
    -- FIXME: Why Vector.dist is not in scope here ?
    rl = self:getRadius()
    local d = c - p
    local dl = d:len()

    local alpha = math.asin(rl / dl)

    local startv = d:rotated(alpha)  + p
    local endv   = d:rotated(-alpha) + p

    local vertices = {}
    vertices[1] = startv.x
    vertices[2] = startv.y
    vertices[3] = endv.x
    vertices[4] = endv.y
    endv   = endv   + (endv - p) * shadowSize
    startv = startv + (startv - p) * shadowSize
    vertices[5] = endv.x
    vertices[6] = endv.y
    vertices[7] = startv.x
    vertices[8] = startv.y

    return vertices
end

function ChainShape:castShadow()
    return
end

return Shadow
