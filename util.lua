function insideRect_center(centerx, centery, width, height, x, y)
    local startx = centerx - width/2
    local endx   = centerx + width/2
    local starty = centery - height/2
    local endy   = centery + height/2

    return startx <= x and endx >= x and starty <= y and endy >= y
end

function dist(a, b)
    local dx = a.x - b.x
    local dy = a.y - b.y
    return math.sqrt(dx * dx + dy * dy)
end
